# Summary
This example addresses a simple artificial intelligence problem in a high-performance computing environment: GPU calculations are performed to forecast the indoor temperature in an experimental building. However, a high-performance computing (HPC) environment offers capabilities that are not available on individual workstations: parametric studies , i.e., many computations can be launched and processed using arrays of HPC tasks running on multiple GPUs in parallel. Since most HPC is equipped with managers such as (Slurm, Torqueue), the user does not need to think about resource allocation, unlike a personal computer. Since training neural networks may require the installation of many libraries such as CUDA, TensorFlow, etc., Using singularity containers widely used in HPC can simplify the process of launching computations. This example shows how HPC can be used to accelerate the creation of a neural network model for building climate forecast, how to use a container for GPU computations, visualize and compare the results of various neural network models. 

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**
# Table of contents
[[_TOC_]]
# Introduction 
Today, due to the increase in the number of people in cities and the development of infrastructure, the total number of buildings is also increasing. In turn, the energy efficiency of buildings can have a significant impact on both the economy and the climate. Based on [[ref](https://www.worldgbc.org/sites/default/files/2018%20GlobalABC%20Global%20Status%20Report.pdf)], buildings account for 39% of total greenhouse gas emissions and consume about 40% of total global energy consumption [[ref](https://www.sciencedirect.com/science/article/pii/S0378778816305783)]. According to statistics from [[ref](https://www.researchgate.net/publication/11864405_The_National_Human_Activity_Pattern_Survey_NHAPS_A_resource_for_assessing_exposure_to_environmental_pollutants)],people spend 86.9% of their time inside different types of buildings. Consequently, buildings significantly affect the quality of human life and the ecology of the earth. As early as the last century, people began to optimize building performance using a variety of planning management techniques. However, much progress has been made recently and artificial intelligence (AI) are being integrated into building management tools that are able to independently assess building microclimate dynamics and other variables over time. One of the possible AI control algorithms is model-based predictive control  [[ref](https://www.sciencedirect.com/science/article/pii/S0957417417306565)]. The method is based on selecting the set of optimal control parameters of building systems according to the efficiency theory which uses the indoor climate forecasts for optimal parameter choose. Here one can see how the forecast of indoor climate conditions plays an important role, because the level of internal comfort and energy efficiency of buildings depends on its accuracy. For time series forecasting several neural network architectures such as [LSTM](https://www.bioinf.jku.at/publications/older/2604.pdf), [TCN](https://medium.com/unit8-machine-learning-publication/temporal-convolutional-networks-and-forecasting-5ce1b6e97ce4), [Dense](https://www.researchgate.net/publication/341786016_Applications_of_Artificial_Neural_Networks_in_Greenhouse_Technology_and_Overview_for_Smart_Agriculture_Development) could be used. To achieve better performance of any neural network model several hyperparameter settings  such as  [batch size](https://arxiv.org/pdf/1609.04836.pdf), [learning rate](https://machinelearningmastery.com/understand-the-dynamics-of-learning-rate-on-deep-learning-neural-networks/) and [delta](https://en.wikipedia.org/wiki/Huber_loss) should be tested.

# Problem background




This  demo case shows an example of creating a neural network model predicting the internal temperature inside the AEP experimental stand located in the botanical garden of the university of Latvia. The description of the monitoring system and the location of the sensors, the data of which were used to train the neural network, can be found [here](https://www.researchgate.net/publication/320658066_Monitoring_results_and_analysis_of_thermal_comfort_conditions_in_experimental_buildings_for_different_heating_systems_and_ventilation_regimes_during_heating_and_cooling_seasons) and [here](https://gitlab.com/eurocc-latvia/bms-ai-model/-/blob/master/Dataset_plots/README.md). The temperature changes in the building can be seen in the graph below.

<img src="Pictures/BD_temp.PNG" alt="drawing" width="700"/>

One can see in the plot above that the temperature in the room oscillates over time. This can occur because the predefined PID (proportional–integral–derivative) coefficients in the thermostat do not adequately describe the current state of building, which in turn leads to incorrect operating parameters of the heat pump. Heat pump efficiency depends on the temperature difference between its cooling and heating units. Consequently, a timely reduction in heat pump energy output based on a forecast can reduce temperature oscillations which would reduce energy consumption.  Damping the oscillations would also improve thermal comfort. To study this case, were crated 3 different working principal models RNN, convolutional and Dense in TensorFlow open-source platform.
To study this case, 3 different models (RNN, Convolution and Dense) were implemented in the TensorFlow platform. Further reading on this particular models example can be found here [LSTM](https://www.bioinf.jku.at/publications/older/2604.pdf), [TCN](https://medium.com/unit8-machine-learning-publication/temporal-convolutional-networks-and-forecasting-5ce1b6e97ce4), [Dense](https://www.researchgate.net/publication/341786016_Applications_of_Artificial_Neural_Networks_in_Greenhouse_Technology_and_Overview_for_Smart_Agriculture_Development).

This example considers training  above mentioned models under different hyperparameter settings  such as batch size, learning rate and delta to achieve better forecast quality. Two plots below show an example of difference between different hyperparameter settings  in context of LSTM model: 


<img src="Pictures/BD_h_parm.PNG" alt="drawing" width="700"/>



# Contents of this case

Here is the overview of all files in this repository. The contents and use of these files will be described in detail further in this document.
* `Pictures` — Folder containing images for this *readme* file.
* `Dataset_plots` — Folder containing plots of all dataset variables.
* `Lib` — Folder containing libriaries `tcn.py` and `Load_model.py` for this case  .
* `Conv` — Folder containing  files for convolutional model TCN  
  * `ConvModel.py` — Model file for convolutional neural network where defined inmut and autput data lenghts data prepocesing and model arhitecture.
  * `QsubConv.sh` — Bash script where resources are specified for one TCN model training in Torque.
* Dense — Folder containing  files for model Dense  
  * `DenseModel.py` — Model file for convolutional neural network where defined inmut and autput data lenghts data prepocesing and model arhitecture.
  * `QsubDense.sh` — Bash script where resources are specified for one Dense model training in Torque.
* RNN — Folder containing  files for model RNN  
  * `RNNModel.py` — Model file for convolutional neural network where defined inmut and autput data lenghts data prepocesing and model arhitecture.
  * `QsubRNN.sh` — Bash script where resources are specified for one RNN model training in Torque.
* `AnaliseIteration.py` — script to sumerise results of all models in one folder in one file .
* `Use_trained_model.py` — script to use pretrained models.
* `RunConvTaskArray.sh` — script to launch traning of several TCN models.
* `RunDenseTaskArray.sh` — script to launch traning of several Dense models.
* `RunRNNTaskArray.sh` — script to launch traning of several RNN models.
* `Download.sh` — script whitch  Download heavy files and folbers: 
  * `Dense_trained_model` — Pretrained Dense model folber.
  * `Conv_trained_model` — Pretrained TCN model folber.
  * `RNN_trained_model` — Pretrained RNN model folber   
  * `container.sif` — container with CUDA , cuDNN and ather python flibriaries required for thease case.
  * `Dataset.csv` — Dataset fot this case.
* `README.md` — This *readme* file.

<!--- 

| Name                | Description                                                           |
| ------------------- | --------------------------------------------------------------------- |
| Sensors in building |                                                                       |
| AER-POW.1           | Instantaneous active power for heat pump ventilation (W)              |
| AER-POW.2           | Instantaneous active power of the heat pump electric heater (W)       |
| AER-POW.3           | Instantaneous active power consumed by other devices in the room (W)  |
| AER-TH-AIR.LOFT     | Attic sensor   Temp(C)/humidity(%)                                    |
| AER-TH-CEILING      | Ceiling sensor attached to the outer wool layer Temp(C)/ humidity (%) |
| AER-TH-FLOOR        | Floor sendor behind the outer wool layer Temp(C)/ humidity (%)        |
| AER-TH-WALL.CONSTR  | Sensor in the slab cavity (middle) Temp(C)/ humidity (%)              |
| AER-TH-WALL.WOOL    | Wall sensor just outside the stone wool layer Temp(C)/ humidity (%)   |
| AER-TH-WINDOW       | Sensor at the window frame Temp(C)/ humidity (%)                      |
| AER-TH-DOOR         | Sensor at the door frame Temp(C)/ humidity (%)                        |
| AER-TH-AIR.UNDER    | Sensor beneath the building Temp(C)/ humidity (%)                     |
| AER-TH-AIR.FAC      | Sensor in the ventilated facade Temp(C)/ humidity (%)                 |
| AER-TH-AIR.ROOM-1   | Sensor in the middle of the room. h=0.1m below the ceiling.           |
| AER-TH-AIR.ROOM-2   | Sensor in the middle of the room. h=1.7 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-3   | Sensor in the middle of the room. h=1.1 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-4   | Sensor in the middle of the room. h=0.6 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-5   | Sensor in the middle of the room. h=0.1 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-6   | Sensor 0.1 m from the outer wall. h=1.1                               |
| AER-TH-AIR.ROOM-ADD | Temp/humidity sensor in the room                                      |
| AER-Q-WALL          | Heat flux density at the inner surface of the outer wall              |
| AER-V-AIR.ROOM-1    | Air flow at the heat pump  (cm/s)                                     |
| AER-V-AIR.ROOM-2    | Air flow in the middle of the room (cm/s)                             |
| AER-V-AIR.ROOM-3    | Air flow in the ventilated facade (cm/s)                              |
| AER-SOL-AIR.ROOM    | Solar radiation sensor by the window (W/m2)                           |
| Meteo station       |                                                                       |
| MET-TH-AIR.OUT      | Outer Temp(C)/humidity(%)                                             |
| MET-W.VEL-AIR.OUT   | Outdoor wind speed                                                    |
| MET-W.DIR-AIR.OUT   | Outdoor wind direction (Deg)                                          |
| MET-P-AIR.OUT       | Outdoor air pressure (mBar)                                           |
| MET-SOL-AIR.OUT     | Outdoor Solar radiation intensity (W/m2)                              |

![Instance Segmentation Sample](Pictures/Sensor_location.PNG)

## How it works

The first step in NN training is data preparation. Looking at the Dataset.csv set, it can be seen that almost every sensor has a data loss rate of ~ 3% which is a rather typical problem for long-term data monitoring. Linear interpolation can be used to replace NaN values and a built-in interpolate() function from the Pandas library was used for this purpose. The  describe() function can be used to inspect the datasets: 

![Instance Segmentation Sample](Pictures/Describe_info.PNG)

It can be seen that the values vary greatly while NN models work best with numbers in the range from 0 to 1, which is the typical working range of the activation functions. Therefore the datasets must be normalized – one first subtracts the minimum measured value from the dataset entries to avoid negative and/or excessive values, and then divides all entries by the maximum value. In this case, when using the UNX format, the last value will be 1, therefore the values beyond 1 will be out of range of the activation function. Time is an important parameter because its values depend on a number of processes such as the motion of the Sun across the sky and its effective radiation output. To understand what frequencies are dominant, apply the discrete Fourier transform (DFT) to the data:

![Instance Segmentation Sample](Pictures/RFFT.PNG)

It can be seen that the dominant frequencies are the daily and annual cycles. To represent the time for NN input, trigonometric formulae can be used to encode the annual/daily cycle phases. The code is as follows:
```
df['Day cos'] = np.cos(timestamp_s * (2*np.pi / day)) #Create column of day phases
df['Year cos'] = np.cos(timestamp_s * (2*np.pi / year)) #Create column of year phases
```
Another value that must be adjusted after normalization is the wind direction (MET-W.DIR-AIR.OUT). It can be noted that the wind direction varies from 0 to 359 degrees that are mapped to 0 and 1, respectively, during normalization. Plotting the wind speed versus direction one can see the following distribution:

![Instance Segmentation Sample](Pictures/wind_deg.PNG)

Note that the start and end values of the distribution are very close, which is expected because the directions almost coincide. To better represent the wind data they can be transformed into vector form:
```
wv = df.pop('MET-W.VEL-AIR.OUT: (m/s)') #remove wind speed data from df and sve in variable
wd_rad = df.pop('MET-W.DIR-AIR.OUT: (Degrees)')*np.pi / 180 #convert data to radians
df['Wx'] = wv*np.cos(wd_rad)  #wind derection x component
df['Wy'] = wv*np.sin(wd_rad)    #wind derection y component
```

![Instance Segmentation Sample](Pictures/wind_vec.PNG)

The wind speed in the x or y direction is now reflected in each wind component. Now one may normalize the data, but first it is important to split the data sets into training and validation parts. Training data is used to find the optimal weights, whereas the validation data sets are used to check whether the model overfits  the data. The code for dataset separation into training/validation sets and subsequent normalization is as follows:

```
n = len(df)
train_df = df[0:int(n*0.9)]  #split data for train
val_df = df[int(n*0.9):]  #split data for validation
train_min = train_df.min()   #search for minimal value in each column
train_df = (train_df - train_min)  #makes minimal value of any parameter 0
val_df = (val_df - train_min) #makes minimal value of any parameter 0
train_max = train_df.max() #
train_df = train_df/train_max #normalise all values from 0 to 1
val_df = val_df/train_max #normalise all values from 0 to 1
```
The data then needs to be divided into Input data and Lable data. Input data will later be fed to the input NN layers, while Lable data will be used by the NN as a reference for further optimization. A dataset_creation function was created for this purpose:
```
def dataset_creation(dataframe, Coloums_lable_name,coloums_input_name,row_past_count,row_future_count)
```
`Dataframe` is the already normalized data set, `coloums_input_name` is the list of names for parameters that the algorithm will use as input data, `Coloums_lable_name` is the list of parameters that the algorithm predicts; `row_past_count` and `row_future_count` are integer parameters that describe the history range to be considered by the NN model and how far into the future to will predict (minutes). The principle of operation of the Dataset creation function is shown below:

![Instance Segmentation Sample](Pictures/data_split.PNG)


3 NN models were created for this demo: Dense, Recurrent Neural Network (RNN) and Convolution Neural Network (CNN):

### DENSE

Today, there exist numerous NN architectures for development of NN models. One of the simplest examples is dense sequential. In this model, each neuron is connected to all neurons in adjacent layers. This demonstration was created in a 4-layer architecture. The first layer in this architecture converts the input matrix into a vector, then each input value is connected to each neuron in the following layers, the number of which is equal to the number of input data. The data from the first dense layer is transferred to the next one similarly. The second dense layer contains a number of neurons equal to the desired number of output values . The last layer in this network restructures the data so that they can be compared with the lable dataset. This network does not have many hidden layers, but those layers are narrow enough, so if, for example, the input of sensors is collected for 3 hours and all sensor data is used, prediction for 30 minutes only involves >3.5⋅10^7 parametrs.  If one considers 12 hours, the number of parameters increases to ~ 7⋅10^8 solving the problem requires a lot of GPU memory. It might seem like averaging the data can reduce the amount of data, but actually it can corrupt the data. For instance,  averaging the wind data after a year will yield mean wind speed close to 0 – this will not be the case, generally, for higher resolution data. Different techniques can be used to reduce the amount of data, but it is not clear in general how one or another method will affect the quality of the forecast, so different models need to be compared.
For this model, the stochastic gradient (SGD) was used as an optimization algorithm. This is the oldest optimization algorithm in the history of neural networks. The basic principle of operation is the optimization of weights using a gradient of the loss function, the direction of which depends on the NN output for a number of examples the number of which is called batch size. Batch size affects NN model accuracy and convergence rate. At some point, it may seem that using the entire data set as a batch can significantly speed up the training process, but most likely the model will congest at local minima as seen in the example below:

![Instance Segmentation Sample](Pictures/optimizator_batch_efect.PNG)


The second important hyperparameter of SGD is the lerning rate which prescribes how strongly SGD changes weights. This parameter also affects the training speed as well as the convergence of the model. If this number is too great the model may not converge whereas if this number is too small the convergence rate could be very slow and the model may become congested at the local minimum. Unfortunately, the optimal hyperparameters are initially unknown and several iterations are required to determine them. For this demonstration it was found after several iterations that with a batch size of 100 and a learning rate of 0.01 one has fast and good results. The mean square error loss function is used here because it is necessary to estimate the accuracy of several points simultaneously. Unfortunately, this metric does not always adequately indicate how well the model performs the forecast. As an example, if the output value is a long line that has one localized peak where the model predicts a straight line, the mean square error can be very small. Therefore, it is important to visualize the data during training. For these purposes, each model has 3 callback functions which generate plots for each 100 epochs and gif with all plots at the end of training. Each of these functions visualizes the operation of NN on a single data sample, which can be changed at will to visualize the data sample of interest. 
```
x1 = For_gif(datasemple_number,Model_folber)
x2 = For_gif(datasemple_number,Model_folber)
x3 = For_gif(datasemple_number,Model_folber)
```

Example:
```
x1 = For_gif(520,Model_folber)
```

### RNN

The principle of operation for RNN differs from the previous one. Nowadays RNNs are frequently applied to time series. This method differs from the previous one in the sequence of input data. In a simplified example, when information from one sensor is fed to the input, the long short-term memory (LSTM) layer of RNN models sequentially processes the data (see below):

![Instance Segmentation Sample](Pictures/RNN.PNG)

Here as the Adam optimization algorithm was used which is different from SGD in that its learning rate is not fixed and during training these hyperparameters can change for each weight. It was found in several experiments that this model converges well with the batch size 5000. Note that this model runs significantly slower than the previous one because here there are many unparalleled processes that have a significant negative impact on GPU performance. However according to the tests it still be useful to use GPU as training on that ~20 times faster than CPU.

### Convolutional

The idea behind this model is to use convolution filters to recognize relationships that may contain information about predicted parameters. Conventional convolution filters for the 1-dimensional case use a vector in which the values are known in advance and by means of which filtering takes place. In convolutional neural networks, the values of this vector are searched for during training. In this demonstration one convolution layer with 105 filters with a filter size of 3 is used as the input layer. As with the previous model, optimized Adam with similar hyper parameters was used here.

### Optimal parameter detection

As mentioned above, many factors can affect the accuracy of NNs. One of these is the input data. It is important to provide the network with the set of parameters to be measured which would contain as much information as possible about the state of the building while free of unnecessary information. It is therefore important to understand what parameters to consider. Reducing the number of parameters can improve the accuracy of the NN and reduce the computational cost of running a trained model. One can notice that data sets contains measurements with very similar values  (e.g. AER-TH-AIR.ROOM-5 and AER-TH-AIR.ROOM-4). Significance and substitutability for some of the data are not clear, however. The NN model can find non-trivial relationships it is not an easy task to understand what data should be used in cases with large complex systems. Therefore, this demonstration is dedicated to analyzing the influence of input parameters. In each architecture of this demo, the first iteration trains models with a data set that is missing one element from the coloums_input_name list. This way several models are created for which the algorithm forms separate folders with an iteration number (in this case iteration_0). The iteration folder contains several folders with models, the folder name of each model is the corresponding model ID. When all combinations are tested, the models are compared and an iteration folder is formed for the comparison graph (see below):

![Instance Segmentation Sample](Pictures/Iteration_summary.PNG)

Then a new iteration folder is formed where a similar operation is repeated, but in this case the parameter with minimum impact on the prediction accuracy is removed from the coloums_input_name list. This process continues while the coloums_input_name list has a number of parameters greater than the threshold defined by the min_input_parm variable. In the parameter reduction end program creates summary where best model of each iteration is compared.

![Instance Segmentation Sample](Pictures/Iteration_comparison.PNG)


!-->


# Runnning the case

## Obtaining the case

Download this case directly from GitLab by executing the command below. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
```
git clone https://gitlab.com/eurocc-latvia/bms-ai-model.git
```
To load heavy files like Dataset.csv, trained models and singularity container, run the following command in directory `bms-ai-model`:

```
bash Download.sh
```

<!---

## Running the machine learning algorithm on a workstation

To start training one of the models on a personal computer in directory `bms-ai-model`, execute the following command:

```
singularity exec --nv container.sif python ModelFolber/ModelPythonFile.py Delta BatchSize LerningRate ID
```
Where :

`ModelFolber`- is one of the folowing directories:  `Dense` , `RNN`, `Conv`

`ModelPythonFile.py` - is a model python file name  

!-->

## Running the machine learning algorithm on HPC (Torque)

To start training one of the models in directory `bms-ai-model`, execute one of the following lines:

```
bash RunConvTaskArray.sh          # to start traning TCN model
bash RunDenseTaskArray.sh         # to start traning Dense model
bash RunRNNTaskArray.sh           # to start traning LSTM model
```


## Array Bash script content

To put for training several models with different hyperparameter configuration in a Torque queue in this example is used  `for loop` which automatically create several tasks in specified grid , see RunConvTaskArray.sh code as example:
```
ID=1
for Delta in 0 0.25 0.5 0.75 1               #change Delta value from 0 to 1 with step of 0.25
do
   for LerningRate in 0.001 0.0005 0.0001    #change LerningRate value from 0.001 to 0.0001 with step of 0.0005
   do
      for BatchSize in 50 100 256 500        #change BatchSize value acording to the list of values [50,100,256,500]
      do     
         qsub -v Delta="$Delta",BatchSize="$BatchSize",LerningRate="$LerningRate",ID="$ID" -N ConvModelID$ID Conv/QsubConv.sh #sudmit task to Torque 
         ID=$[$ID +1]
      done
   done
done
```
This Bash script submit to the Torque manager all possible model training configurations in range of: `Delta` from 0 to 1 with step of 0.25, `learning rate`  from 0.001 to 0.0001 with step of 0.0005 and `Batch Size` values (50,100,256,500). To give unique name of each process $ID as a counter is used.




## Torque script contents — Header
To specify resourses and modules  whitch is required for traning of each model Torque script is used, see `QsubDense.sh` code, located in `Dense` folder as example:
```
#!/bin/sh
#PBS -q batch                                          # Specifies type of task line
#PBS -l nodes=1:ppn=4:gpus=1 -l feature=v100,mem=12g   # Specifies what for one model training is required one node with 4 cores , one GPU Tesla v100 and 12GB of memory 
#PBS -l walltime=01:00:00                              # Time limit, hrs:min:sec
```


## Torque script contents — Loading software modules


To simplify the process of launching this example a Singularity container is used with all preinstalled libraries. To load singularity container module the following command is used:


```
module load singularity/3.2.1  

```
Please pay attention that in your HPC cluster these module could have different title.

##  Torque script contents — Calling the main application


```
singularity exec --nv container.sif python Dense/DenseModel.py ${Delta} ${BatchSize} ${LerningRate} ${ID}
```

Where: `singularity exec`- run a command within a container, `--nv` -set to use GPU  ,`container.sif`- set path to container file ,`python Dense/DenseModel.py` - excutes `DenseModel.py` file with python,`${Delta} ${BatchSize} ${LerningRate} ${ID}`- passes  parameters from `Array Bash script` to python file.


##  Using the machine learning algorithm for data prediction

To use trained model Use_trained_model.py script could be used. As usage of trained models does not require much time it could be executed in interactive mode of Torque to do so execute the following command :
```
qsub -I -l nodes=1:ppn=2:gpus=1 -l feature=v100
```
Then load  singularity module with the following command:
```
module load singularity/3.2.1
```
`Use_trained_model.py` require several input parameters:


First is `dataset path` this is the full path to dataset file or name of dataset file if it is located in same directory for example `Dataset.csv`; Second `title` is a name of title for generated plot; Third is a `flag` for visualization  of input data  ,`1` if you want to see input data or `0` to visualize only prediction and measured values; Fourth is `model folder  path` from min case directory; Fifth is `id of datasemple`- script splits dataset for input intervals according to input data length which were used during training. By default,  input length  of trained modules is 180 minets. If `id of datasemple` is set to 0 it means that script will use first 180 minutes of dataset to predict temperature values.   

To excite script with container, use same procedure as was shown in Torque script, see example below:
```
singularity exec --nv container.sif python Use_trained_model.py Dataset.csv "Name of title" 1 Dense_trained_model 500
```
As a result, script use dense model provided with case to create a prediction plot with input, predicted and mesuret values with title 'Name of title' , the prediction is don for 500 data sample. See resul plot beloww:

<img src="Pictures/Name of title.png" alt="drawing" width="700"/>




# Examining trained models

Running one of the TaskArray.sh files in the model folder will create an iteration folder that will contain all trained models. For example, after running `RunConvTaskArray.sh`, an iteration_1 folder is created in the Conv folder, in which folders for all trained models with various hyperparameters specified in RunConvTaskArray.sh are created. The name of each model folder corresponds to the ID created in `RunConvTaskArray.sh`.

A `Parameters.csv` is generated in each model folder, in which is seen the metrics change during training in the mean_absolute_error, mean_absolute_error_Validation, MeanSquaredError, MeanSquaredError_Validation, RSquare, RSquare_Validation, RootMeanSquaredError_Validation columns. This file also contains information about the hyperparameters that were used to train this model, as well as the time spent on this model, the number of parameters in the architecture, the epoch number, the length of the input and output time series, the final estimation metrics on data  that were not used during training.  The model folder also contains 3 folders `100`, `250`, `520` in which the model's intermediate prediction plots  are rendered during training.

To simplify evoluation of multiple models all models results coud be merget in one file with the folowing comands runed in `bms-ai-model` folber:

```
module load singularity/3.2.1  
singularity exec  container.sif python AnaliseIteration.py Dense/iteration_1   
```
Last line runs `AnaliseIteration.py` python script and passes  to it path from `bms-ai-model` folder to iteration folder  with models which should be evaluated in these case it is Dense\iteration_1 . After executing this comands `Summary.csv` file will be created in Dense\iteration_1 folber. `iteration_1.csv` will contain all final evaluation metrics and hiperparameter configuration of each model in iteration folder.
 


<!---


## How to run

Tenserflow v2.3.1 was used for this demonstration. NN model training could be significantly accelerated with the GPU. However, for this library to work with the GPU, several other libraries are needed that may be difficult to install. This procedure could be greatly simplified when working at dedicated computational centers. To run any of the models one can use Singularity Container which a package of installed libraries that unlike the docker does not require root privileges.. 
To load the container dataset and pretrained models run :
```
bash Download.sh
```

Before training it is necessary to load modules for container and library package Anconda for Python. This can be done with the following commands:

```
module load singularity/3.2.1 
module load anaconda/conda-4.7.10
```

To start training one of the demo models  the following command must be executed wher last parameter coud be changed acording to selected model ( Dense/RUN_dense.py || Conv/RUN_Conv.py  || RNN/RUN_dense.py ):

```
singularity exec --nv container.sif python Dense/RUN_dense.py
```
-->
# Adjusting machine learning algorithm

At the beginning  of each Model.py file( `DenseModel.py` || `DenseModel.py`  || `DenseModel.py` ), If yoy want you can change length of input time, length of predicted output  as well as list of starting parameters which  NN will use during training  and parameters which must be predicted. See example below:

```
Input_lengh_min=180  #define input history lenght in minets
prediction_lenght=30  #define output  lenght in minets
Coloums_lable_name= ['AER-T-AIR.ROOM-3 (degC)'] #value list for prediction

coloums_input_name=['AER-SOL-AIR.ROOM (W/m2)', 'AER-POW.1  (W)', 'AER-POW.2  (W)',
       'AER-POW.3  (W)', 'AER-H-AIR.LOFT (%)', 'AER-T-AIR.LOFT (degC)',
       'AER-H-FLOOR (%)', 'AER-T-FLOOR (degC)', 'AER-T-WALL.CONSTR (degC)', 'AER-H-WALL.WOOL (%)',
       'AER-H-WINDOW (%)', 'AER-T-WINDOW (degC)', 'AER-H-AIR.FAC (%)',
       'AER-T-AIR.FAC (degC)', 'AER-H-AIR.ROOM-1 (%)','AER-T-AIR.ROOM-1 (degC)',
       'AER-V-AIR.ROOM-1 (cm/s)', 'AER-V-AIR.ROOM-2 (cm/s)',
       'AER-V-AIR.ROOM-3 (cm/s)', 'MET-P-AIR.OUT: (mBar)',
       'MET-H-AIR.OUT.: (%)', 'MET-T-AIR.OUT: (DegC)',
       'MET-SOL-AIR.OUT (W/m2)', 'Wx', 'Wy',  'Day cos',
       'Year cos']


```
By default all models use all available data in dataset and predict temperature in room which in dataset is marked as 'AER-T-AIR.ROOM-3 (degC)'.

# Adjusting dataset for machine learning

For experienced users could  be used another dataset , but it could  require more changes in Model.py files as new dataset could have different structure of data, another titles of columns , data could require additional preprocessing and removing existing preprocessing code part if new dataset do not contain specified data. As an example, in this demo case for a better representation of the wind, direction and speed values are converted to vector format for better representation to the neural network, see code responsible  for it below:
```
wv = df.pop('MET-W.VEL-AIR.OUT: (m/s)') #remove wind speed data from df and sve in variable
wd_rad = df.pop('MET-W.DIR-AIR.OUT: (Degrees)')*np.pi / 180 #convert data to radians
df['Wx'] = wv*np.cos(wd_rad)  #wind derection x vector component
df['Wy'] = wv*np.sin(wd_rad)    #wind derection y vector component

```

If dataset does not contain wind data, code listed above should be removed.
<!--
# References <a name="ref"></a>

1.	Abergel T, Dean B, Dulac J, Hamilton I, Wheeler T. Global Status Report - Towards a zero-emission, efficient and resilient buildings and construction sector; 2018. ISBN 978‐92‐807‐3729‐5. 
2.	Cao, Xiaodong & Xilei, Dai & Liu, Junjie. (2016). Building energy-consumption status worldwide and the state-of-the-art technologies for zero-energy buildings during the past decade. Energy and Buildings. 128. 10.1016/j.enbuild.2016.06.089. 
3.	Klepeis, Neil & Nelson, William & Ott, Wayne & Robinson, John & Tsang, Andy & Switzer, Paul & Behar, Joseph & Hern, Stephen & Engelmann, William. (2001). The National Human Activity Pattern Survey (NHAPS): A resource for assessing exposure to environmental pollutants. Journal of exposure analysis and environmental epidemiology. 11. 231-52. 10.1038/sj.jea.7500165. 
4.	Ruiz, Luis & Rueda, Ramón & Cuéllar, Manuel & Pegalajar Jiménez, Maria del Carmen. (2017). Energy consumption forecasting based on Elman neural networks with evolutive optimization. Expert Systems with Applications. 92. 10.1016/j.eswa.2017.09.059. 
5.	N. S. Keskar, D. Mudigere, J. Nocedal, M. Smelyanskiy, & P. T. P. Tang, “On large-batch training for deep learning: Generalization gap and sharp minima,” 2017. 
6.	Cengil, Emine & Çinar, Ahmet & Guler, Zafer. (2017). A GPU-based convolutional neural network approach for image classification. 1-6. 10.1109/IDAP.2017.8090194. 

-->
