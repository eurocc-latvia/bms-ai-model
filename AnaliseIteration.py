#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
import os
import sys

def analise_iteration(final_folber_path):
    directory_contents = os.listdir(os.getcwd()+'/'+final_folber_path)
    df=pd.read_csv(final_folber_path+'/'+directory_contents[0]+'/Parameters.csv')
    input_c=df['input_values']
    df_all=df[['ID', 'time', 'parametrnumber_t', 'parametrnumber_n',
           'layers', 'epoch', 'input_shape', 'output_shape','Delta','BatchSize','LerningRate',
           'final_evaluation_loss_t', 'final_evaluation_MAE_t',
           'final_evaluation_loss_V', 'final_evaluation_MAE_V','MeanSquaredError','MeanSquaredError_V','RSquare','RSquare_V','RootMeanSquaredError','RootMeanSquaredError_V']]
    df_all=df_all[:1]
    directory_contents.remove(directory_contents[0])
    for x in directory_contents:
        df=pd.read_csv(final_folber_path+'/'+x+'/Parameters.csv')
        tdf_to_append=df[['ID', 'time', 'parametrnumber_t', 'parametrnumber_n',
               'layers', 'epoch', 'input_shape', 'output_shape','Delta','BatchSize','LerningRate',
               'final_evaluation_loss_t', 'final_evaluation_MAE_t',
               'final_evaluation_loss_V', 'final_evaluation_MAE_V','MeanSquaredError','MeanSquaredError_V','RSquare','RSquare_V','RootMeanSquaredError','RootMeanSquaredError_V']]
        tdf_to_append=tdf_to_append[:1]
        df_all=df_all.append(tdf_to_append)
    df_all=df_all.sort_values('RSquare_V', ascending=False)
    df_all.to_csv(os.getcwd()+'/'+final_folber_path+'/'+'Summary.csv',index=False)  ###save
    print("Done!")
    print('All models evaluation is seen in' +os.getcwd()+'/'+final_folber_path+'/'+'Summary.csv')
    return 1

analise_iteration(sys.argv[1])
