#!/bin/sh
#PBS -q batch
#PBS -l nodes=1:ppn=4:gpus=1 -l feature=v100,mem=12g
#PBS -l walltime=00:59:00

module load singularity/3.2.1
cd $PBS_O_WORKDIR

singularity exec --nv container.sif python Dense/DenseModel.py ${Delta} ${BatchSize} ${LerningRate} ${ID}
