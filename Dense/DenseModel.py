#!/usr/bin/env python
# coding: utf-8
import os
import sys
LIBRARYPATH = os.path.abspath(os.path.split(__file__)[0])
print(LIBRARYPATH)
sys.path.append(os.path.abspath(LIBRARYPATH))
from PIL import Image
import IPython
import IPython.display
import seaborn as sns
import tensorflow as tf
from datetime import datetime
import numpy as np
import datetime
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import json
import tensorflow_addons as tfa


#### Basic settings ####
PlotInterval=100
Input_lengh_min=180  #define input history lenght in minets
prediction_lenght=30  #define output  lenght in minets
Coloums_lable_name= ['AER-T-AIR.ROOM-3 (degC)'] #value list for prediction

coloums_input_name=['AER-SOL-AIR.ROOM (W/m2)', 'AER-POW.1  (W)', 'AER-POW.2  (W)',
       'AER-POW.3  (W)', 'AER-H-AIR.LOFT (%)', 'AER-T-AIR.LOFT (degC)',
       'AER-H-FLOOR (%)', 'AER-T-FLOOR (degC)', 'AER-T-WALL.CONSTR (degC)', 'AER-H-WALL.WOOL (%)',
       'AER-H-WINDOW (%)', 'AER-T-WINDOW (degC)', 'AER-H-AIR.FAC (%)',
       'AER-T-AIR.FAC (degC)', 'AER-H-AIR.ROOM-1 (%)',
       'AER-T-AIR.ROOM-1 (degC)', 'AER-H-AIR.ROOM-3 (%)',
       'AER-T-AIR.ROOM-3 (degC)', 'AER-H-AIR.ROOM-5 (%)',
       'AER-T-AIR.ROOM-5 (degC)', 'AER-H-AIR.ROOM-6 (%)',
       'AER-T-AIR.ROOM-6 (degC)', 'AER-Q-WALL (W/m2)',
       'AER-V-AIR.ROOM-1 (cm/s)', 'AER-V-AIR.ROOM-2 (cm/s)',
       'AER-V-AIR.ROOM-3 (cm/s)', 'MET-P-AIR.OUT: (mBar)',
       'MET-H-AIR.OUT.: (%)', 'MET-T-AIR.OUT: (DegC)',
       'MET-SOL-AIR.OUT (W/m2)', 'Wx', 'Wy',  'Day cos',
       'Year cos']  # all input values

#### Advanced settings ####
Epoch_number=1000   # number of epoch to train
data_each_interval=1  #if GPU memory is not enough is possible take into account only x value but it increase mean absolute error
neurons_multiplaer=1 #multiplayer for neurons count
ID=0  # start  model ID
start_from=0   #iteration
current_path=os.getcwd()+"/Dense"   #or teke current

#### Dataset preparation ####

df = pd.read_csv('Dataset.csv')   #reads file with input data

df=df[::data_each_interval]  ### takes each X value

date_time = pd.to_datetime(df.pop('Timestamp'), format='%Y-%m-%d %H:%M:%S') #convert time to unix time and remove temestamp column

df=df.interpolate()#iterpolate Nan if exist

wv = df.pop('MET-W.VEL-AIR.OUT: (m/s)') #remove wind speed data from df and sve in variable

wd_rad = df.pop('MET-W.DIR-AIR.OUT: (Degrees)')*np.pi / 180 #convert data to radians

df['Wx'] = wv*np.cos(wd_rad)  #wind derection x component
df['Wy'] = wv*np.sin(wd_rad)    #wind derection y component

timestamp_s = date_time.map(datetime.datetime.timestamp)
from datetime import datetime
import time

day = 86400     #minets in day
year = (365.2425)*day   #minets in year

df['Day cos'] = np.cos(timestamp_s * (2*np.pi / day)) #Create column of day phases
df['Year cos'] = np.cos(timestamp_s * (2*np.pi / year)) #Create column of year phases


n = len(df)
train_df = df[0:int(n*0.9)]  #split data for train
val_df = df[int(n*0.9):]  #split data for validation


train_min = train_df.min()   #search for minimal value in each column
train_df = (train_df - train_min)  #makes minimal value of any parameter 0
val_df = (val_df - train_min) #makes minimal value of any parameter 0
train_max = train_df.max() #
train_df = train_df/train_max #normalise all values from 0 to 1
val_df = val_df/train_max #normalise all values from 0 to 1

def dataset_creation(dataframe, Coloums_lable_name,coloums_input_name,row_past_count,row_future_count):
    window_wide=row_past_count+row_future_count   #calculate lenght for split of dataset
    cut=len(dataframe)-len(dataframe)%window_wide   #calculate where to cut dataframe
    dataframe=dataframe[:cut]   # cut dataframe to fit in windows
    df_arr_in=np.array(dataframe[coloums_input_name],dtype=np.float16) #convert from pandas series to np.array
    df_arr_in=df_arr_in.reshape(df_arr_in.shape[0]//window_wide ,window_wide,df_arr_in.shape[1])
    input_my=df_arr_in[:,:row_past_count,:]
    shape_input=np.shape(input_my)          # create input data
    df_arr_L=np.array(dataframe[Coloums_lable_name],dtype=np.float16) #convert from pandas series to np.array
    df_arr_L=df_arr_L.reshape(df_arr_L.shape[0]//window_wide ,window_wide,df_arr_L.shape[1])
    lable_my=df_arr_L[:,row_past_count:,:]
    shape_lable=np.shape(lable_my) # create lable data
    print(np.shape(lable_my),'LABLE')
    num_features=len(Coloums_lable_name)
    return input_my ,lable_my, num_features

def plot_model_save(model,input_c,index,input_my_rep ,lable_my_rep,LAbles_name,value_to_plot,input_data,folber,legend):
    column_number=LAbles_name.index(value_to_plot)
    x=input_my_rep[1+index:3+index]
    all_input_coloumns=x[1] #second input from selected batch (all input columns)

    x=model.predict(x)

    input_len=np.shape(input_my_rep[1+index:3+index])[1]  #correct len of input

    predict_my=pd.DataFrame(x[1])#second prediction from selected batch
    predict_my=predict_my[column_number]

    ymin = 0
    ymax = 30#max(real*train_std[value_to_plot]+train_mean[value_to_plot])+100

    if input_data:

        lable=pd.DataFrame(lable_my_rep[1+index:3+index][1])
        lable=lable[column_number]
        total_lenght=len(lable)+input_len
        count = np.arange(0.0, total_lenght, 1)
        real = lable
        predict_my = predict_my#lable
        fig, ax = plt.subplots()
        input =np.full(total_lenght, np.nan)
        input_values_for_plot=all_input_coloumns[:,input_c[value_to_plot]]#chose input part for plot
        input[:input_len]=input_values_for_plot#.to_numpy() #change begining of nan vector to input values
        lable=np.full(total_lenght, np.nan)
        lable[input_len:]=real#.to_numpy()#change tail of nan vector to input values
        predict=np.full(total_lenght, np.nan)
        predict[input_len:]=predict_my#.to_numpy()#change tail of nan vector to input values
        input=input*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization
        xmax = len(predict)
        ax=plt.axes(xlim=(0, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))#change size
        ax.plot(count,input,label='Input '+value_to_plot)

    else:
        lable=pd.DataFrame(lable_my_rep[1+index:3+index][1])
        lable=lable[column_number]
        total_lenght=len(lable)
        count = np.arange(0.0, total_lenght, 1)
        real = lable
        predict_my = predict_my#lable
        fig, ax = plt.subplots()
        lable=real#.to_numpy()#change tail of nan vector to input values
        predict=predict_my#.to_numpy()#change tail of nan vector to input values
        xmax = len(predict)
        ax=plt.axes(xlim=(0, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))#change size




    predict=predict*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization
    lable=lable*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization
    ax.scatter(count,lable,label='Mesured '+value_to_plot,c='black',s=10)
    ax.plot(count,predict, marker='.',label=' predict '+value_to_plot,c='#ff7f0e')
    ax.set(xlabel='steps', ylabel='renormalised', title=legend)
    plt.legend(loc='best')
    ax.grid()
    plt.savefig(folber, dpi=1000)
    plt.close()



#### Model traning ####
def NN_train(input_lenght,lable_lenght, input_c,output_c,path,id,epochs_n,neurons_multiplaer,Delta,BatchSize,LerningRate):
    coloums_input_name=input_c
    Coloums_lable_name= output_c
    row_past_count=input_lenght
    row_future_count=lable_lenght
    input_c = {name: i for i, name in enumerate(coloums_input_name)}
    input_my_rep ,lable_my_rep,num_features=dataset_creation(train_df,
                                                             Coloums_lable_name,
                                                             coloums_input_name,
                                                             row_past_count,
                                                             row_future_count)


    input_my_rep_val ,lable_my_rep_vall,num_features=dataset_creation(val_df,
                                                             Coloums_lable_name,
                                                             coloums_input_name,
                                                             row_past_count,
                                                             row_future_count)
    class For_gif(tf.keras.callbacks.Callback):
        def __init__(self, element, folber):
            self.element = element #define traning set part for plot
            self.folber = folber  #difine folber
            try:
                os.mkdir(folber+'/'+str(element))
            except:
                print('exist')
            self.folber = folber+'/'+str(element)

        def on_epoch_end(self, epoch, logs={}):

            # make lot every x epoch
            if epoch > 0 and epoch%PlotInterval==0:
                plot_model_save(self.model,input_c,
                    self.element ,
                    input_my_rep
                    ,lable_my_rep
                    ,Coloums_lable_name
                    ,Coloums_lable_name[0],
                    False,self.folber+'/'+str(epoch)+'epoch.png','Epoch_'+str(epoch)+' Dataset part '+str(self.element))
        def on_train_end(self, logs=None):
            images  = os.listdir(self.folber)
            for x,i in enumerate(images):
                images[x]=i.replace("epoch.png", "")
            images.sort(key=int)

            fp_out = self.folber+'/'+str(self.element)+"_GIF.gif"

            img, *imgs = [Image.open(self.folber+'/'+f+'epoch.png') for f in images]
            img.save(fp=fp_out, format='GIF', append_images=imgs,
                     save_all=True, duration=300, loop=1,dpi=(1000, 1000))



    final= "/"+str(ID)#model folber
    Model_folber=path+"/"+str(ID)  #model full path


    try:
        os.mkdir(Model_folber)
    except:
        print('exist')
    x1 = For_gif(100,Model_folber)
    x2 = For_gif(250,Model_folber)
    x3 = For_gif(520,Model_folber)





    weight_name='Separate_test'
    period_for_save=epochs_n//5
    model_my = tf.keras.Sequential([

        tf.keras.layers.Flatten(),

        tf.keras.layers.Dense(len(coloums_input_name)*row_past_count*neurons_multiplaer, activation='sigmoid'),

        tf.keras.layers.Dense(row_future_count*num_features,kernel_initializer=tf.initializers.Zeros()),

        tf.keras.layers.Reshape([row_future_count, num_features])
    ])



    json.dump(model_my.to_json(), open(Model_folber+'/'+'data.json', 'w'))   #save model in jason







    model_my.compile(loss=tf.keras.losses.Huber(Delta),optimizer=tfa.optimizers.RectifiedAdam(lr=LerningRate),metrics=[tf.metrics.MeanAbsoluteError(),tf.metrics.MeanSquaredError(),tfa.metrics.RSquare(y_shape=(30,1)),tf.keras.metrics.RootMeanSquaredError()])


    start=time.time() #start computing time
    with tf.device('/GPU:0' ):  # if you whant to run on GPU
        history_1 = model_my.fit(input_my_rep,lable_my_rep,      #train model
                      batch_size=BatchSize,
                      epochs=epochs_n,
                      validation_split=0.2,
                      shuffle=False,
                      verbose=1,callbacks=[x1,x2,x3])#mc])




    time_l=time.time()-start
    my_array = np.array([[ID,time_l]])
    df = pd.DataFrame(my_array, columns = ['ID','time'])





    df['parametrnumber_t']=np.sum([np.prod(v.get_shape()) for v in model_my.trainable_weights])
    print(df['parametrnumber_t'],' Parameters')
    df['parametrnumber_n']=np.sum([np.prod(v.get_shape()) for v in model_my.non_trainable_weights])
    df['layers']=len(model_my.layers)
    #df['norm']='(x-min)/max(x-min)'
    df['epoch']=epochs_n
    #df['each(min)']=data_each_interval
    df['input_shape']=np.shape(input_my_rep)[1]
    df['output_shape']=np.shape(lable_my_rep)[1]
    df['Delta']=Delta
    df['BatchSize']=BatchSize
    df['LerningRate']=LerningRate



    df['final_evaluation_loss_t']=model_my.evaluate(input_my_rep ,lable_my_rep, verbose=0)[0]
    df['final_evaluation_MAE_t']=model_my.evaluate(input_my_rep ,lable_my_rep, verbose=0)[1]
    df['MeanSquaredError']=model_my.evaluate(input_my_rep ,lable_my_rep, verbose=0)[2]
    df['RSquare']=model_my.evaluate(input_my_rep ,lable_my_rep, verbose=0)[3]
    df['RootMeanSquaredError']=model_my.evaluate(input_my_rep ,lable_my_rep, verbose=0)[4]


    df['final_evaluation_loss_V']= model_my.evaluate(input_my_rep_val ,lable_my_rep_vall)[0]
    df['final_evaluation_MAE_V']= model_my.evaluate(input_my_rep_val ,lable_my_rep_vall)[1]
    df['MeanSquaredError_V']= model_my.evaluate(input_my_rep_val ,lable_my_rep_vall)[2]
    df['RSquare_V']= model_my.evaluate(input_my_rep_val ,lable_my_rep_vall)[3]
    df['RootMeanSquaredError_V']= model_my.evaluate(input_my_rep_val ,lable_my_rep_vall)[4]


    df1 = pd.DataFrame({'input_values':coloums_input_name})
    df=pd.concat([df,df1], axis=1)


    df1 = pd.DataFrame({'output_values':Coloums_lable_name})
    df=pd.concat([df,df1], axis=1)

    mean_absolute_error=history_1.history['mean_absolute_error']
    df1 = pd.DataFrame({'MeanAbsoluteErrorr_Validation':mean_absolute_error})
    df=pd.concat([df,df1], axis=1)
    val_mean_absolute_error=history_1.history['val_mean_absolute_error']
    df1 = pd.DataFrame({'MeanAbsoluteErrorr_Validation':val_mean_absolute_error})
    df=pd.concat([df,df1], axis=1)

    mean_squared_error=history_1.history['mean_squared_error']
    df1 = pd.DataFrame({'MeanSquaredError':mean_squared_error})
    df=pd.concat([df,df1], axis=1)
    val_mean_squared_error=history_1.history['val_mean_squared_error']
    df1 = pd.DataFrame({'MeanSquaredError_Validation':val_mean_squared_error})
    df=pd.concat([df,df1], axis=1)


    r_square=history_1.history['r_square']
    df1 = pd.DataFrame({'RSquare':r_square})
    df=pd.concat([df,df1], axis=1)
    val_r_square=history_1.history['val_r_square']
    df1 = pd.DataFrame({'RSquare_Validation':val_r_square})
    df=pd.concat([df,df1], axis=1)


    root_mean_squared_error=history_1.history['root_mean_squared_error']
    df1 = pd.DataFrame({'RootMeanSquaredError_Validation':root_mean_squared_error})
    df=pd.concat([df,df1], axis=1)
    val_root_mean_squared_error=history_1.history['val_root_mean_squared_error']
    df1 = pd.DataFrame({'RootMeanSquaredError_Validation':val_root_mean_squared_error})
    df=pd.concat([df,df1], axis=1)


    df.to_csv(Model_folber+'/Parameters.csv')  #save model in tf format
    model_my.save(Model_folber+'/my_model')  #save model in tf format
    tf.keras.backend.clear_session()


iteration=1
iteration=iteration+start_from
final= "/iteration_"+str(iteration)
iteration_folber=current_path+final
try:
    os.mkdir(iteration_folber)
except:
    print('Folber exist!')


DeltaList=[0,0.25,0.5,0.75,1]
BatchSizeList=[20,32,50,100,256,400,500,700]
LerningRateList=[0.001,0.0005,0.0001]


Delta=float(sys.argv[1])

BatchSize=int(sys.argv[2])

LerningRate=float(sys.argv[3])

ID=int(sys.argv[4])

NN_train(Input_lengh_min,prediction_lenght, coloums_input_name,Coloums_lable_name,iteration_folber,ID , Epoch_number ,neurons_multiplaer,Delta,BatchSize,LerningRate)
