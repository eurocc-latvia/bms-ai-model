#!/bin/sh
#PBS -q batch
#PBS -l nodes=1:ppn=2:gpus=1 -l feature=v100,mem=12g
#PBS -l walltime=02:00:00
echo "${Delta}"
echo "${BatchSize}"
echo "${LerningRate}"
echo "${ID}"

module load singularity/3.2.1
cd $PBS_O_WORKDIR

singularity exec --nv container.sif python RNN/RNNModel.py ${Delta} ${BatchSize} ${LerningRate} ${ID}
