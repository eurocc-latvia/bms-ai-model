import Lib.Load_model as Load_model
import sys

Model_folber='Conv_trained_model' #path to model
CSV_file_path='Dataset.csv'# path to  dataset
Name_of_plot_file='Name_As_You_Whant.png'
title_of_plot='Temperature change in room' #set title name
value_to_pradict='AER-T-AIR.ROOM-3 (degC)'  # choise of parameter to plot
show_input_data=True #set False if you do not want to see input data sequeence
dataset_sample_number=520

CSV_file_path=sys.argv[1]# path to  dataset
Name_of_plot_file=sys.argv[2]+'.png'
title_of_plot=sys.argv[2]#set title name
show_input_data=int(sys.argv[3]) #set False if you do not want to see input data sequeence
Model_folber=sys.argv[4] #path to model
dataset_sample_number=int(sys.argv[5])


#save_point_name='00000400.h5'
#model,input_t ,lable_t,input_m_val,lable__vall,config=Load_model.load_model_from_specific_save_point(Model_folber,save_point_name,CSV_file_path)
model,input_t ,lable_t,input_m_val,lable__vall,input_c,output_c,norm_cof_mi,norm_cof_ma=Load_model.load_final_model(Model_folber,CSV_file_path)


input_number = {name: i for i, name in enumerate(input_c)}
Load_model.plot_model_save(model,input_number,
                dataset_sample_number ,  # set diferet time interval
                input_t
                ,lable_t
                ,output_c,
                value_to_pradict,# choise of parameter to plot
                show_input_data,  #set True if yo want to see input data sequeence
                Name_of_plot_file,  #
                title_of_plot,True,norm_cof_mi,norm_cof_ma)
