#!/bin/bash
ID=1
for Delta in 0.1 0.25 0.5 0.75 1
do
   for LerningRate in 0.001 0.0005 0.0001
   do
         for BatchSize in 50 100 256 500
      do
         qsub -v Delta="$Delta",BatchSize="$BatchSize",LerningRate="$LerningRate",ID="$ID" -N ConvModelID$ID Conv/QsubConv.sh
         ID=$[$ID +1]
      done
   done
done
