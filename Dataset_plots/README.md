# Description of sensor data in the dataset
Dataset provided with case contain the folowing title names listed below. 

| Title Name                | Description                                                           |
| ------------------- | --------------------------------------------------------------------- |
| Sensors in building |                                                                       |
| AER-POW.1           | Instantaneous active power for heat pump ventilation (W)              |
| AER-POW.2           | Instantaneous active power of the heat pump electric heater (W)       |
| AER-POW.3           | Instantaneous active power consumed by other devices in the room (W)  |
| AER-TH-AIR.LOFT     | Attic sensor   Temp(C)/humidity(%)                                    |
| AER-TH-CEILING      | Ceiling sensor attached to the outer wool layer Temp(C)/ humidity (%) |
| AER-TH-FLOOR        | Floor sendor behind the outer wool layer Temp(C)/ humidity (%)        |
| AER-TH-WALL.CONSTR  | Sensor in the slab cavity (middle) Temp(C)/ humidity (%)              |
| AER-TH-WALL.WOOL    | Wall sensor just outside the stone wool layer Temp(C)/ humidity (%)   |
| AER-TH-WINDOW       | Sensor at the window frame Temp(C)/ humidity (%)                      |
| AER-TH-DOOR         | Sensor at the door frame Temp(C)/ humidity (%)                        |
| AER-TH-AIR.UNDER    | Sensor beneath the building Temp(C)/ humidity (%)                     |
| AER-TH-AIR.FAC      | Sensor in the ventilated facade Temp(C)/ humidity (%)                 |
| AER-TH-AIR.ROOM-1   | Sensor in the middle of the room. h=0.1m below the ceiling. Temp(C)/ humidity (%)          |
| AER-TH-AIR.ROOM-2   | Sensor in the middle of the room. h=1.7 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-3   | Sensor in the middle of the room. h=1.1 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-4   | Sensor in the middle of the room. h=0.6 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-5   | Sensor in the middle of the room. h=0.1 m Temp(C)/ humidity (%)       |
| AER-TH-AIR.ROOM-6   | Sensor 0.1 m from the outer wall. h=1.1  Temp(C)/ humidity (%)                             |
| AER-TH-AIR.ROOM-ADD | Temp/humidity sensor in the room Temp(C)/ humidity (%)                                     |
| AER-Q-WALL          | Heat flux density at the inner surface of the outer wall (W/m2)             |
| AER-V-AIR.ROOM-1    | Air flow at the heat pump  (cm/s)                                     |
| AER-V-AIR.ROOM-2    | Air flow in the middle of the room (cm/s)                             |
| AER-V-AIR.ROOM-3    | Air flow in the ventilated facade (cm/s)                              |
| AER-SOL-AIR.ROOM    | Solar radiation sensor by the window (W/m2)                           |
| Meteo station       |                                                                       |
| MET-TH-AIR.OUT      | Outer Temp(C)/humidity(%)                                             |
| MET-W.VEL-AIR.OUT   | Outdoor wind speed (m/s)                                                   |
| MET-W.DIR-AIR.OUT   | Outdoor wind direction (Deg)                                          |
| MET-P-AIR.OUT       | Outdoor air pressure (mBar)                                           |
| MET-SOL-AIR.OUT     | Outdoor Solar radiation intensity (W/m2)                              |


Sensor location coud be seen in figure listed below:

![Instance Segmentation Sample](Pictures/Sensor_location.PNG)

