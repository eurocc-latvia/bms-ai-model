#!/bin/bash
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1juWAeFqst6Ap-pkrymE1pWYaU3quDpdE' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1juWAeFqst6Ap-pkrymE1pWYaU3quDpdE" -O container.sif && rm -rf /tmp/cookies.txt

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1zNSmvfN9YUolxLQqU_PPt0yOlHmFotD6' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1zNSmvfN9YUolxLQqU_PPt0yOlHmFotD6" -O Dataset.csv && rm -rf /tmp/cookies.txt

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1O-qmlYrhT5NiDf_oxGtCEhkJ2Z8xDCug' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1O-qmlYrhT5NiDf_oxGtCEhkJ2Z8xDCug" -O Trained_models.zip && rm -rf /tmp/cookies.txt

unzip Trained_models.zip

rm Trained_models.zip
