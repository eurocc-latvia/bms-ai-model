#!/usr/bin/env python
# coding: utf-8


import IPython
import IPython.display
import seaborn as sns
import tensorflow as tf
from datetime import datetime
import tensorflow as tf
import numpy as np
import os
import datetime
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
from datetime import datetime
import json
import tensorflow_addons as tfa
import Lib.tcn



def dataset_creation(dataframe, Coloums_lable_name,coloums_input_name,row_past_count,row_future_count):
    window_wide=row_past_count+row_future_count
    cut=len(dataframe)-len(dataframe)%window_wide
    dataframe=dataframe[:cut]   # cut dataframe to fit in windows
    df_arr_in=np.array(dataframe[coloums_input_name],dtype=np.float16)
    df_arr_in=df_arr_in.reshape(df_arr_in.shape[0]//window_wide ,window_wide,df_arr_in.shape[1])
    input_my=df_arr_in[:,:row_past_count,:]
    shape_input=np.shape(input_my)          # create input data
    print(np.shape(input_my),'INPUT')
    df_arr_L=np.array(dataframe[Coloums_lable_name],dtype=np.float16)
    df_arr_L=df_arr_L.reshape(df_arr_L.shape[0]//window_wide ,window_wide,df_arr_L.shape[1])
    lable_my=df_arr_L[:,row_past_count:,:]
    shape_lable=np.shape(lable_my) # create lable data
    print(np.shape(lable_my),'LABLE')
    num_features=len(Coloums_lable_name)
    return input_my ,lable_my, num_features
def plot_model_save(model,input_c,index,input_my_rep ,lable_my_rep,LAbles_name,value_to_plot,input_data,folber,legend,plot,norm_cof_mi,norm_cof_ma):
    column_number=LAbles_name.index(value_to_plot)
    #print(column_number,'ATENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    x=input_my_rep[1+index:3+index]
    all_input_coloumns=x[1] #second input from selected batch (all input columns)
    input_values_for_plot=all_input_coloumns[:,input_c[value_to_plot]]#chose input part for plot
    x=model.predict(x)

    input_len=np.shape(input_my_rep[1+index:3+index])[1]  #correct len of input

    predict_my=pd.DataFrame(x[1])#second prediction from selected batch
    predict_my=predict_my[column_number]

    print(np.shape(predict_my)[0])

    ymin = 16
    ymax = 24#max(real*train_std[value_to_plot]+train_mean[value_to_plot])+100
    train_min,train_max=norm_cof_mi,norm_cof_ma

    if input_data:

        lable=pd.DataFrame(lable_my_rep[1+index:3+index][1])
        lable=lable[column_number]
        total_lenght=len(lable)+input_len
        count = np.arange(0.0, total_lenght, 1)
        real = lable
        predict_my = predict_my#lable
        fig, ax = plt.subplots()
        input =np.full(total_lenght, np.nan)
        input[:input_len]=input_values_for_plot#.to_numpy() #change begining of nan vector to input values
        lable=np.full(total_lenght, np.nan)
        lable[input_len:]=real#.to_numpy()#change tail of nan vector to input values
        predict=np.full(total_lenght, np.nan)
        predict[input_len:]=predict_my#.to_numpy()#change tail of nan vector to input values
        input=input*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization
        xmax = len(predict)
        ax=plt.axes(xlim=(0, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))#change size
        ax.plot(count,input,label='Input '+value_to_plot)

    else:
        lable=pd.DataFrame(lable_my_rep[1+index:3+index][1])
        lable=lable[column_number]
        total_lenght=len(lable)
        count = np.arange(0.0, total_lenght, 1)
        real = lable
        predict_my = predict_my#lable
        fig, ax = plt.subplots()

        lable=real#.to_numpy()#change tail of nan vector to input values

        predict=predict_my#.to_numpy()#change tail of nan vector to input values


        xmax = len(predict)
        ax=plt.axes(xlim=(0, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))#change size




    predict=predict*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization
    lable=lable*train_max[value_to_plot]+train_min[value_to_plot] #reverse normalization


    ax.scatter(count,lable,label='Mesured '+value_to_plot,c='black',s=10)

    ax.plot(count,predict, marker='.',label=' predict '+value_to_plot,c='#ff7f0e')
    ax.set(xlabel='Time(min)', ylabel='renormalised', title=legend)
    plt.legend(loc='best')
    ax.grid()
    plt.savefig(folber, dpi=1000)
    if plot:
        plt.show()



def load_model_from_specific_save_point(Model_folber,save_point_name,CSV_file_path):
    df = pd.read_csv(CSV_file_path)
    config=pd.read_csv(Model_folber+'/Parameters.csv')
    each=int(list(config["each(min)"])[0])
    input_shape=int(list(config["input_shape"])[0])
    output_shape=int(list(config["output_shape"])[0])

    input_values=config.input_values.unique()
    input_values = [x for x in input_values if pd.isnull(x) == False and x != 'nan']

    output_values=config.output_values.unique()
    output_values = [x for x in output_values if pd.isnull(x) == False and x != 'nan']
    try:
        date_time = pd.to_datetime(df.pop('Timestamp'), format='%Y-%m-%d %H:%M:%S')
    except:
        print('Timestamp column does not exist')


    df=df.interpolate()
    try:
        wv = df.pop('MET-W.VEL-AIR.OUT: (m/s)')
        wd_rad = df.pop('MET-W.DIR-AIR.OUT: (Degrees)')*np.pi / 180# Convert to radians.
        df['Wx'] = wv*np.cos(wd_rad)# Calculate the wind x and y components.
        df['Wy'] = wv*np.sin(wd_rad)
    except:
        print('wind data does not exist')

    try:
        timestamp_s = date_time.map(datetime.timestamp)
        day = 24*60*60
        year = (365.2425)*day
        df['Day sin'] = np.sin(timestamp_s * (2 * np.pi / day))
        df['Day cos'] = np.cos(timestamp_s * (2 * np.pi / day))
        df['Year sin'] = np.sin(timestamp_s * (2 * np.pi / year))
        df['Year cos'] = np.cos(timestamp_s * (2 * np.pi / year))
    except:
        print('timestamp_does_not_exist')
    n = len(df)
    train_df = df[0:int(n*0.9)]
    val_df = df[int(n*0.9):]
    train_min = train_df.min()
    train_df = (train_df - train_min)
    val_df = (val_df - train_min)
    train_max = train_df.max()
    train_df = train_df/train_max
    val_df = val_df/train_max

    coloums_input_name=input_values
    Coloums_lable_name= output_values
    row_past_count=input_shape
    row_future_count=output_shape
    input_c = {name: i for i, name in enumerate(coloums_input_name)}
    input_my_rep ,lable_my_rep,num_features=dataset_creation(train_df,
                                                             Coloums_lable_name,
                                                             coloums_input_name,
                                                             row_past_count,
                                                             row_future_count)
    input_my_rep_val ,lable_my_rep_vall,num_features=dataset_creation(val_df,
                                                         Coloums_lable_name,
                                                         coloums_input_name,
                                                         row_past_count,
                                                         row_future_count)
    config2 = json.load(open(Model_folber+'/data.json'))#############################################################
    model_new=tf.keras.models.model_from_json(config2)



    new_model = tf.keras.models.load_model(Model_folber+'/my_model',custom_objects={"RSquare": tfa.metrics.RSquare})#load model from jason
    new_model.load_weights(Model_folber+'/'+save_point_name)
    return new_model,input_my_rep ,lable_my_rep,input_my_rep_val,lable_my_rep_vall,config


def load_final_model(Model_folber,CSV_file_path):
    df = pd.read_csv(CSV_file_path)
    config=pd.read_csv(Model_folber+'/Parameters.csv')
    each=int(list(config["each(min)"])[0])
    input_shape=int(list(config["input_shape"])[0])
    output_shape=int(list(config["output_shape"])[0])

    input_values=config.input_values.unique()
    input_values = [x for x in input_values if pd.isnull(x) == False and x != 'nan']

    output_values=config.output_values.unique()
    output_values = [x for x in output_values if pd.isnull(x) == False and x != 'nan']
    try:
        date_time = pd.to_datetime(df.pop('Timestamp'), format='%Y-%m-%d %H:%M:%S')
    except:
        print('Timestamp column does not exist')


    df=df.interpolate()
    try:
        wv = df.pop('MET-W.VEL-AIR.OUT: (m/s)')
        wd_rad = df.pop('MET-W.DIR-AIR.OUT: (Degrees)')*np.pi / 180# Convert to radians.
        df['Wx'] = wv*np.cos(wd_rad)# Calculate the wind x and y components.
        df['Wy'] = wv*np.sin(wd_rad)
    except:
        print('wind data does not exist')

    try:
        timestamp_s = date_time.map(datetime.timestamp)
        day = 24*60*60
        year = (365.2425)*day
        df['Day sin'] = np.sin(timestamp_s * (2 * np.pi / day))
        df['Day cos'] = np.cos(timestamp_s * (2 * np.pi / day))
        df['Year sin'] = np.sin(timestamp_s * (2 * np.pi / year))
        df['Year cos'] = np.cos(timestamp_s * (2 * np.pi / year))
    except:
        print('timestamp_does_not_exist')
    n = len(df)
    train_df = df[0:int(n*0.9)]
    val_df = df[int(n*0.9):]
    train_min = train_df.min()
    norm_cof_mi=train_min
    train_df = (train_df - train_min)
    val_df = (val_df - train_min)
    train_max = train_df.max()
    norm_cof_ma=train_max
    train_df = train_df/train_max
    val_df = val_df/train_max


    coloums_input_name=input_values
    Coloums_lable_name= output_values
    row_past_count=input_shape
    row_future_count=output_shape
    input_c = {name: i for i, name in enumerate(coloums_input_name)}
    input_my_rep ,lable_my_rep,num_features=dataset_creation(train_df,
                                                             Coloums_lable_name,
                                                             coloums_input_name,
                                                             row_past_count,
                                                             row_future_count)
    input_my_rep_val ,lable_my_rep_vall,num_features=dataset_creation(val_df,
                                                         Coloums_lable_name,
                                                         coloums_input_name,
                                                         row_past_count,
                                                         row_future_count)


    new_model = tf.keras.models.load_model(Model_folber+'/my_model', custom_objects={"RSquare": tfa.metrics.RSquare })#load model from jason

    input_shape=list(config["input_values"])
    output_shape=list(config["output_values"])
    input_values=config.input_values.unique()
    input_c = [x for x in input_values if pd.isnull(x) == False and x != 'nan']
    output_values=config.output_values.unique()
    output_c = [x for x in output_values if pd.isnull(x) == False and x != 'nan']

    return new_model,input_my_rep ,lable_my_rep,input_my_rep_val,lable_my_rep_vall,input_c,output_c,norm_cof_mi,norm_cof_ma
